This repository collects the code and data used in verifying Maeda's
conjecture on modular forms for weights up to 12000, as presented in the
paper *Experimental evidence for Maeda's conjecture on modular forms* by
A. Ghitza and A. McAndrew.

The algorithm was implemented in [Sage], an open-source mathematical
software system based on the Python language and tying together a large
number of mathematical libraries.

## The data
The results of the computations are stored in the two directories data
and data-consec.  They are stored in text files, one for each weight
that was tested.  The directory data holds the results of running the
randomized version of the algorithm (as described in our paper), while
the directory data-consec holds the results of running the consecutive
version of the algorithm (as described in the 1999 paper of Conrey and
Farmer -- see our paper for a precise reference).

#### The format of the result files
Each result file starts with some metadata:

* the weight of the space
* the total time taken by the computation
* the time taken to compute the Victor Miller basis
* the time taken to compute the matrix of the Hecke operator T2
* the time taken to find a prime of each type I, II, and III, and the
  corresponding number of necessary attempts
* the version of Sage used in the computation
* the machine used for the computation
* the date/time when the computation ended

The actual results follow.  For each of the types I, II, and II, a prime
*p*
of that type is given, together with the reduction modulo *p* of the
characteristic polynomial of T2.


## How to use the code
Coming up.

(We plan to clean the code up, add
some examples/doctests, and some comments.)

[Sage]: http://sagemath.org/
